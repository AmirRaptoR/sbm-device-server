﻿using System;

namespace SimballServer
{
    class Program
    {
        static int Main(string[] args)
        {
            Console.WriteLine("Initializing device");
            var count = SbmUtil.Init();
            Console.WriteLine($"{count} device[s] been initialized");
            Console.WriteLine(
                $"Device Version : {SbmUtil.GetDeviceVersion(0)} - Device Name : {SbmUtil.GetDeviceDescription(0)} - Dll Version : {SbmUtil.GetDllVersion()}");
            Console.WriteLine("Initializing was successfull");
            Console.WriteLine($"Creating tcp server on port {Properties.Settings.Default.Port}");
            try
            {
                new TcpServer(Properties.Settings.Default.Port).StartListening();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Console.WriteLine("Can not create tcp server. Press any key to exit");
                Console.ReadKey();
                return 1;
            }

            Console.WriteLine("Server running");


            return 0;
        }
    }
}