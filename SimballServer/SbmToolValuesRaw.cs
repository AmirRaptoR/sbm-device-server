﻿namespace SimballServer
{
    public struct SbmToolValuesRaw
    {
        public int InstrTypeFiltered { get; set; } // raw (but filtered) value for instrument id (10 bits)

        public int InstrTypeDecoded { get; set; } // fully decoded instrument id
        public int InstrTypeStable { get; set; } // stable value based on several consecutive readings
        public int NumStableInstrTypeReadings { get; set; } // Counter for consecutive readings

        public ushort Ins { get; set; } // raw 14-bit
        public ushort Hnd { get; set; } // raw 10-bit
        public ushort Aux { get; set; } // raw 10-bit

        public double InsScaled { get; set; } // insertion scaled to [0,1]
        public double HndScaled { get; set; } // handle    scaled to [0,1]
        public double AuxScaled { get; set; } // auxiliary scaled to [0,1]

        public double InsScaledFiltered { get; set; }
        public double HndScaledFiltered { get; set; }
        public double AuxScaledFiltered { get; set; }
    }
}