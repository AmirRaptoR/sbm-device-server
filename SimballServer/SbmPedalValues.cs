﻿namespace SimballServer
{
    public struct SbmPedalValues
    {
        public byte Left { get; set; }
        public byte Right { get; set; }
    }
}