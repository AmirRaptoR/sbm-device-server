﻿namespace SimballServer
{
    public struct SbmDeviceValues
    {
        public double Theta { get; set; } // angle in radians (from front to back, pi/2 in center)
        public double Phi { get; set; } // angle in radians (from right to left, pi/2 in center)
        public double Gamma { get; set; } // angle in radians (counter-clockwise rotation about axis, [-pi,pi])

        public SbmPedalValues PedalA { get; set; }
        public SbmPedalValues PedalB { get; set; }

        public SbmToolValues ToolA { get; set; }
        public SbmToolValues ToolB { get; set; }

        public SbmCameraValues ToolCamera { get; set; }

        public SbmStaplerValues ToolStapler { get; set; }

        public ulong RawErrCode { get; set; } // error code from Simball device
    }
}