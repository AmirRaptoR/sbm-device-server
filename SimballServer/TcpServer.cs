﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace SimballServer
{
    class TcpServer
    {
        public int Port { get; }

        public TcpServer(int port)
        {
            Port = port;
        }

        private TcpListener _listener;
        private readonly List<TcpClientHandler> _clients = new List<TcpClientHandler>();

        public void StartListening()
        {
            _listener = TcpListener.Create(Port);
            _listener.Start();
            while (true)
            {
                var socket = _listener.AcceptTcpClient();
                Console.WriteLine("Client connected");
                var tcpClientHandler = new TcpClientHandler(socket);
                _clients.Add(tcpClientHandler);
            }
        }

    }
}