﻿namespace SimballServer
{
    public enum SbmButtonFlags
    {
        Button1 = 1 << 0, // set when button 1 is pressed (on instrument with buttons, e.g. camera)
        Button2 = 1 << 1, // set when button 2 is pressed (on instrument with buttons, e.g. camera)
        Button3 = 1 << 2 // set when button 3 is pressed (on instrument with buttons, e.g. camera)
    }
}