﻿using System.Runtime.InteropServices;

namespace SimballServer
{
    [StructLayout(LayoutKind.Sequential)]
    public struct ClientSettings
    {
        [MarshalAs(UnmanagedType.I1, SizeConst = 4)]
        public byte ReadFrquency;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 5)]
        public string Name;
    }
}