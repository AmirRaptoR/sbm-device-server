﻿namespace SimballServer
{
    public  enum SbmErrorMessage
    {
        NoError = 0, // the operation was successful
        NotInitialized = -1, // the communication was not initialized, call Init()
        AlreadyInitialized = -2, // the communication was already initialized, call Exit() before calling Init() again
        BadDeviceNumber = -3,
        // the device number was out of range (must be between 0 and the number returned by Init())
        DataNotReady = -4, // has not received the first data from device yet, wait and call GetValues() again
        DeviceNotConnected = -5,
        // the device has been disconnected since the call to Init(), call Exit() and Init() to get new device list for connected devices
        DeviceReadError = -6, // general read error for device, try again or reinitialize
        DeviceWriteError = -7, // general write error for device, try again or reinitialize
        RegistryError = -8
        // error reading and/or writing in the Windows registry, everything will work except that the order of devices will be based on the USB port order
    }
}