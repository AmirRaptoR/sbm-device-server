﻿namespace SimballServer
{
    public struct SbmToolValues
    {
        public byte IsValid { get; set; }
        public SbmToolVariant ToolVariant { get; set; }

        public byte IsInserted { get; set; }
        public double Insertion { get; set; } // insertion of the instrument tip in millimeters, 

        public double Handle { get; set; } // a value between 0.0 and 1.0, where 1.0 means released handle

        public short Buttons { get; set; }

        public SbmForceSensorValues ToolForceSensor { get; set; }

        public SbmToolValuesRaw Raw { get; set; } // raw public  values, for debugging
    }
}