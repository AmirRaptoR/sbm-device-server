﻿namespace SimballServer
{
    public  enum SbmToolVariant
    {
        None = 0, // No instrument connected

        GrasperGeneric = 0x0100,
        GrasperAesculap = 0x0101,
        GrasperPeanut = 0x0102,
        GrasperForceps = 0x0103,

        // NOTE: Replaced with separate camera handling...
        CameraFront = 0x0200,
        CameraRear = 0x0300,

        NeedleDriverGeneric = 0x0400,
        NeedleDriverAesculap = 0x0401,

        // NOTE: Replaced with separate stapler handling...
        StaplerFront = 0x0500,
        StaplerRear = 0x0600,

        ShortGrasperGeneric = 0x0A00,

        ShortBoxTrainingGeneric = 0x0B00,

        SuctionIrrigationGeneric = 0x0C00,

        DiathermyHookGeneric = 0x0D00,

        ForceSensor = 0x0E00
    }
}