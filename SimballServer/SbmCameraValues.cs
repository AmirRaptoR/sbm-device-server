﻿namespace SimballServer
{
    public struct SbmCameraValues
    {
        public byte IsValid { get; set; }

        public byte IsInserted { get; set; }
        public double Insertion { get; set; } // insertion of the instrument tip in millimeters, 

        public double CameraRotation { get; set; } // rotation angle for camera in radians, in (-pi, pi)
        public double CameraZoom { get; set; } // zoom vale for camera, between 0.0 and 1.0
        public double CameraFocus { get; set; } // focus value for camera, between 0.0 and 1.0

        public SbmButtonFlags Buttons { get; set; }
    }
}