﻿namespace SimballServer
{
    public struct SbmForceSensorValues
    {
        public byte IsValid { get; set; }

        public double ForceX { get; set; } // the force in Newton in x-direction (to the right)
        public double ForceY { get; set; } // the force in Newton in y-direction (forward)
        public double ForceZ { get; set; } // the force in Newton in z-direction (up, towards user)
    }
}