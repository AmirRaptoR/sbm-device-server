﻿using System;
using System.Runtime.InteropServices;

namespace SimballServer
{
    public static class StructHelper{
        public  static byte[] GetBytes<TStruct>(TStruct str)
        {
            int size = Marshal.SizeOf(str);
            byte[] arr = new byte[size];

            IntPtr ptr = Marshal.AllocHGlobal(size);
            Marshal.StructureToPtr(str, ptr, true);
            Marshal.Copy(ptr, arr, 0, size);
            Marshal.FreeHGlobal(ptr);
            return arr;
        }

        public static TStruct FromBytes<TStruct>(byte[] arr) where TStruct : new()
        {
            TStruct str = new TStruct();

            int size = Marshal.SizeOf(str);
            IntPtr ptr = Marshal.AllocHGlobal(size);

            Marshal.Copy(arr, 0, ptr, size);

            str = (TStruct)Marshal.PtrToStructure(ptr, str.GetType());
            Marshal.FreeHGlobal(ptr);

            return str;
        }
    }
}