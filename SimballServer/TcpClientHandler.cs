﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

namespace SimballServer
{
    public class TcpClientHandler
    {
        private readonly TcpClient _client;

        public TcpClientHandler(TcpClient client)
        {
            _client = client;
            var clientSettingSize = Marshal.SizeOf<ClientSettings>();


            byte[] data = new byte[clientSettingSize];
            ClientIp = ((IPEndPoint)_client.Client.RemoteEndPoint).Address.ToString();
            ClientName = ClientIp;
            ReadFrequency = _client.GetStream().ReadByte();
            if (ReadFrequency < 1)
            {
                ReadFrequency = 1;
            }
            if (ReadFrequency > 100)
            {
                ReadFrequency = 100;
            }
            Console.WriteLine($"Client name : {ClientName} - Read frequency : {ReadFrequency}");
            new Thread(DataReader)
            {
                Name = $"{ClientName}_DataReader"
            }.Start();
        }

        private void DataReader()
        {
            var failures = 0;
            while (true)
            {
                try
                {
                    var cameraParameters = new List<String>();
                    var letf = new List<String>();
                    var right = new List<String>();
                    var pedals = new List<String>();

                    for (int i = 0; i < SbmUtil.DevicesCount; i++)
                    {
                        var values = SbmUtil.GetValues(i);

                        if (values.ToolCamera.IsValid == 1)
                        {
                            cameraParameters.Add((values.Theta / Math.PI * 180.0).ToString("000.000"));
                            cameraParameters.Add((values.Phi / Math.PI * 180.0).ToString("000.000"));
                            cameraParameters.Add((values.Gamma / Math.PI * 180.0).ToString("000.000"));

                            cameraParameters.Add((values.ToolCamera.CameraRotation / Math.PI * 180.0).ToString("000.000"));
                            cameraParameters.Add((values.ToolCamera.Insertion).ToString("000.000"));
                            cameraParameters.Add((values.ToolCamera.CameraZoom).ToString("0.00"));
                            cameraParameters.Add((values.ToolCamera.CameraFocus).ToString("0.00"));
                            cameraParameters.Add((((values.ToolCamera.Buttons & SbmButtonFlags.Button1) == SbmButtonFlags.Button1) ? "1" : "0").ToString());
                            cameraParameters.Add((((values.ToolCamera.Buttons & SbmButtonFlags.Button2) == SbmButtonFlags.Button2) ? "1" : "0").ToString());
                            cameraParameters.Add((((values.ToolCamera.Buttons & SbmButtonFlags.Button3) == SbmButtonFlags.Button3) ? "1" : "0").ToString());
                        }
                        if (values.ToolB.IsValid == 1)
                        {
                            right.Add((values.Theta / Math.PI * 180.0).ToString("000.000"));
                            right.Add((values.Phi / Math.PI * 180.0).ToString("000.000"));
                            right.Add((values.Gamma / Math.PI * 180.0).ToString("000.000"));
                            right.Add((values.ToolB.Insertion).ToString("000.000"));
                            right.Add((values.ToolB.Handle).ToString("0.0000"));
                            pedals.Add(values.PedalB.Left.ToString());
                            pedals.Add(values.PedalB.Right.ToString());
                            pedals.Add(values.PedalA.Left.ToString());
                            pedals.Add(values.PedalA.Right.ToString());
                        }

                    }

                    var retValue = string.Join(",", cameraParameters.ToArray())
                        + "," + string.Join(",", letf.ToArray())
                        + "," + string.Join(",", right.ToArray())
                    + "," + string.Join(",", pedals.ToArray());


                    //var rnd = new Random();
                    //var data = Encoding.ASCII.GetBytes(string.Join(",",
                    //    Enumerable.Range(0, 24).Select(x => rnd.NextDouble().ToString("0000.0000"))));
                    var data = Encoding.ASCII.GetBytes(retValue);
                    Console.WriteLine("Data to send : " + retValue);
                    _client.GetStream().Write(data, 0, data.Length);
                    Thread.Sleep(1000 / ReadFrequency);
                    failures = 0;
                }
                catch (Exception ex)
                {
                    failures++;
                    if (failures == 5)
                    {
                        Console.WriteLine($"Connection with client {ClientName}");
                        break;
                    }

                    Console.WriteLine(
                        $"Can not write device values to network connection ({ClientIp})");
                    Console.WriteLine(ex);
                }
            }
        }


        public int ReadFrequency { get; set; }
        public string ClientIp { get; set; }
        public string ClientName { get; set; }
    }
}