﻿#region

using System;
using System.Runtime.InteropServices;
using System.Text;

#endregion

namespace SimballServer
{
    public static class SbmUtil
    {
        public static int DevicesCount { get; set; }


        public class SimballException : Exception
        {
            public SimballException(string message) : base(message)
            {
            }
        }

        private static void ProcessResult(SbmErrorMessage error)
        {
            if (error == SbmErrorMessage.NoError)
                return;
            throw new SimballException(GetErrorString(error));
        }

        private static string ConvertByteArrayToString(byte[] bytes)
        {
            return Encoding.ASCII.GetString(bytes).Trim((char) 0);
        }

        public static int Init()
        {
#if DEBUG1
            return 0;
#else
            ProcessResult(SBM_Init(out var count));
            DevicesCount = count;
            return count;
#endif
        }

        public static void Exit()
        {
#if DEBUG1
#else
            ProcessResult(SBM_Exit());
#endif
        }

        public static SbmDeviceValues GetValues(int deviceNo)
        {
#if DEBUG1
            var rnd = new Random();
            return new SbmDeviceValues
            {
                Gamma = rnd.NextDouble(),
                PedalA = new SbmPedalValues
                {
                    Left = rnd.NextDouble()>0.5,
                    Right= rnd.NextDouble()>0.5
                },
                PedalB= new SbmPedalValues
                {
                    Left = rnd.NextDouble() > 0.5,
                    Right = rnd.NextDouble() > 0.5
                },
                Phi = rnd.NextDouble(),
                RawErrCode = (ulong) rnd.Next(),
                Theta = rnd.NextDouble()
            };
#else
            ProcessResult(SBM_GetValues(deviceNo, out var values));
            return values;
#endif
        }

        public static string GetDeviceId(int deviceNo)
        {
#if DEBUG1
            return "Simulator";
#else
            var str = new byte[11];
            ProcessResult(SBM_GetDeviceID(deviceNo, str));
            return ConvertByteArrayToString(str);
#endif
        }

        public static string GetDeviceDescription(int deviceNo)
        {
#if DEBUG1
            return "Simulator device";
#else
            var str = new byte[64];
            ProcessResult(SBM_GetDeviceDescription(deviceNo, str));
            return ConvertByteArrayToString(str);
#endif
        }

        public static string GetDllVersion()
        {
#if DEBUG1
            return "V1.0";
#else
            var str = new byte[64];
            SBM_GetDLLVersion(str);
            return ConvertByteArrayToString(str);
#endif
        }

        public static string GetAboutString(int deviceNo)
        {
#if DEBUG1
            return "This is just simulator for debug perpose";
#else
            var str = new byte[256];
            SBM_GetAboutString(str);
            return ConvertByteArrayToString(str);
#endif
        }

        public static string GetDeviceVersion(int deviceNo)
        {
#if DEBUG1
            return "V1.0";
#else
            ProcessResult(SBM_GetDeviceVersion(deviceNo, out var major, out var minor));
            return $"{major}.{minor}";
#endif
        }

        private static string GetErrorString(SbmErrorMessage error)
        {
            var str = new byte[256];
            SBM_GetErrorString(error, str);
            return ConvertByteArrayToString(str);
        }


        public const string DllName = "SimballMedicalHID.dll";

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl,
            EntryPoint = "?SBM_Init@@YA?AW4SBMErrorMessage@@AEAH@Z", CharSet = CharSet.Ansi, SetLastError = true)]
        internal static extern SbmErrorMessage SBM_Init(out int nrdevices);

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl,
            EntryPoint = "?SBM_Exit@@YA?AW4SBMErrorMessage@@XZ", CharSet = CharSet.Ansi, SetLastError = true)]
        internal static extern SbmErrorMessage SBM_Exit();

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl,
            EntryPoint = "?SBM_GetValues@@YA?AW4SBMErrorMessage@@HAEAUSBMDeviceValues@@@Z", CharSet = CharSet.Ansi,
            SetLastError = true)]
        internal static extern SbmErrorMessage SBM_GetValues(int devicenr, out SbmDeviceValues values);

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl,
            EntryPoint = "?SBM_GetDeviceID@@YA?AW4SBMErrorMessage@@HQEAD@Z", CharSet = CharSet.Ansi,
            SetLastError = true)]
        internal static extern SbmErrorMessage SBM_GetDeviceID(int devicenr, byte[] idStr);

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl,
            EntryPoint = "?SBM_GetDeviceDescription@@YA?AW4SBMErrorMessage@@HQEAD@Z", CharSet = CharSet.Ansi,
            SetLastError = true)]
        internal static extern SbmErrorMessage SBM_GetDeviceDescription(int devicenr, byte[] desc);

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl, EntryPoint = "?SBM_GetDLLVersion@@YAXQEAD@Z",
            CharSet = CharSet.Ansi, SetLastError = true)]
        internal static extern void SBM_GetDLLVersion(byte[] versionStr);

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl, EntryPoint = "?SBM_GetAboutString@@YAXQEAD@Z",
            CharSet = CharSet.Ansi, SetLastError = true)]
        internal static extern void SBM_GetAboutString(byte[] aboutStr);

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl,
            EntryPoint = "?SBM_GetDeviceVersion@@YA?AW4SBMErrorMessage@@HPEAF0@Z", CharSet = CharSet.Ansi,
            SetLastError = true)]
        internal static extern SbmErrorMessage SBM_GetDeviceVersion(int devicenr, out short major, out short minor);

        [DllImport(DllName, CallingConvention = CallingConvention.Cdecl,
            EntryPoint = "?SBM_GetErrorString@@YAXW4SBMErrorMessage@@QEAD@Z", CharSet = CharSet.Ansi,
            SetLastError = true)]
        internal static extern void SBM_GetErrorString(SbmErrorMessage err, byte[] errStr);
    }
}