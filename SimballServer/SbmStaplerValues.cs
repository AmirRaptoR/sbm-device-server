namespace SimballServer
{
    public struct SbmStaplerValues
    {
        public byte IsValid { get; set; }

        public byte IsInserted { get; set; }
        public double Insertion { get; set; } // insertion of the instrument tip in millimeters,

        public double Handle { get; set; } // a value between 0.0 and 1.0, where 1.0 means released handle

        public int Angle { get; set; }
        public double Lever { get; set; }
    }
}