﻿using System;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using Newtonsoft.Json;
using SimballServer;
using Test.Properties;

namespace Test
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Thread.Sleep(2000);
            var client = new TcpClient();
            try
            {
                client.Connect(Settings.Default.ServerAddress, Settings.Default.ServerPort);
                Console.WriteLine(
                    $"Connected to server {Settings.Default.ServerAddress}:{Settings.Default.ServerPort}");
            }
            catch (Exception ex)
            {
                Console.WriteLine(
                    $"Can not connect to server {Settings.Default.ServerAddress}:{Settings.Default.ServerPort}");
                Console.WriteLine(ex);
                Console.ReadKey();
                return;
            }

            var setting = new ClientSettings
            {
                ReadFrquency = 1,
                Name = "Test client"
            };
            var settingData = StructHelper.GetBytes(setting);
            client.GetStream().Write(settingData, 0, settingData.Length);


            var sizeToRead = 239; //Marshal.SizeOf<SbmDeviceValues>();
            var buffer = new byte[sizeToRead];

            while (true)
            {
                client.GetStream().Read(buffer, 0, sizeToRead);
//                var values = StructHelper.FromBytes<SbmDeviceValues>(buffer);
                Console.WriteLine("---------------------------------------------------------");
                var stringData = Encoding.ASCII.GetString(buffer);
                Console.WriteLine("Values received : " + stringData);
//                Console.WriteLine("Values received : " + JsonConvert.SerializeObject(values));

                if (Console.KeyAvailable && Console.ReadKey().Key == ConsoleKey.Escape)
                {
                    client.Close();
                    Console.WriteLine("Connection closed");
                    return;
                }
            }
        }
    }
}